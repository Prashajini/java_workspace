
public class StringDemo {
	
	public static void main(String[] args) {
		
		StringClass strcla = new StringClass();
		
		String fName = "Prashajini";
		String lName = "Pratheepan";
		String name3 = "Vijay";
		String fullName = "Prashajini Pratheepan";
		
		System.out.println(strcla.getCharAt(2,  fName));//String charAt() Method
		System.out.println(strcla.Concat(fName,  lName));//String concat() Method
		System.out.println(strcla.getindexOf(name3));//String indexOf() Method
		System.out.println(strcla.getindexOf1(fName));//String indexOf(int ch, int fromIndex)
		System.out.println(strcla.getindexOf2(lName, fullName));//String indexOf(String str) Method
		System.out.println(strcla.getindexOf2(lName, fullName));//String indexOf(String str, int fromIndex)
		System.out.println(strcla.getlastindexOf(fullName));//Java � String lastIndexOf() Method
		System.out.println(strcla.getlastindexOf1(lName));//String lastIndexOf(int ch, int fromIndex)
		System.out.println(strcla.getlastindexOf2(lName, fullName));//String lastIndexOf(String str) Method
		System.out.println(strcla.getlastindexOf3(name3));//Java - String lastIndexOf() fromIndex
		System.out.println(strcla.getlength(fName));//Java - String length() Method
		System.out.println(strcla.getreplace(lName));//Java - String replace() Method
		System.out.println(strcla.startsWith(fullName));//Java - String startsWith() Method
		System.out.println(strcla.substring(fullName));//Java - String substring() Method
		System.out.println(strcla.substring1(fullName));//String substring(beginIndex, endIndex)
		System.out.println(strcla.getToLowerCase(name3));//Java - String toLowerCase() Method
		System.out.println(strcla.getToUpperCase(fName));//Java - String toUpperCase() Method
		System.out.println(strcla.gettrim(fullName));//Java - String trim() Method
	}

}
