
public class StringClass {
	
	

	public char getCharAt(int index, String word) {
		char ch = word.charAt(index);
		return ch;
		
	}
	
	
	public String Concat(String fName, String lName) {
		String concat = fName.concat(lName);
		return concat;
		
		
	}
	
	
	public int getindexOf(String word) {
		int ch = word.indexOf('a');
		return ch;
	}
	
	public int getindexOf1(String word) {
		int ch = word.indexOf('a', 3);
		return ch;
		
	}
	
	public int getindexOf2(String fullName, String lName) {
		int ch = lName.indexOf(fullName);
		return ch;
	}
		
	public int getindexOf3(String fullName, String lName) {
		int ch = fullName.indexOf(lName, 8);
		return ch;
	}
	
	public int getlastindexOf(String word) {
		int ch = word.lastIndexOf('n');
		return ch;
	}
	
	public int getlastindexOf1(String word) {
		int ch = word.lastIndexOf('a', 5);
		return ch;
	}
	
	public int getlastindexOf2(String lName, String fullName) {
		int ch = fullName.indexOf(lName);
		return ch;
	}

	public int getlastindexOf3(String word) {
		int ch = word.lastIndexOf('i', 5);
		return ch;
	}
	public int getlength(String word) {
		int ch = word.length();
		return ch;
	}
	public String getreplace(String word) {
		String ch = word.replace('e' , 'a');
		return ch;
	}
	
	public boolean startsWith(String word) {
		boolean ch = word.startsWith("Prashajini");
		boolean ch1 = word.startsWith("Pratheepan");
		return ch;
	}
	public String substring(String word) {
		String ch = word.substring(11);
		return ch;
	}
	
	public String substring1(String word) {
		String ch = word.substring(11,15);
		return ch;
	}
	public String getToLowerCase(String word) {
		String str = word.toLowerCase();
		return str;
	}
	
	public String getToUpperCase(String word) {
		String str = word.toUpperCase();
		return str;
	}
	public String gettrim(String word) {
		String str = word.trim();
		return str;
	}
}
