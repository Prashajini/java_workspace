package bcas.string;

import bcas.string.StringUppercase;

/**
	 * To perform basic string methods
	 * 
	 * @author Prashajini
	 * @since 18.05.2020
	 * @version 2.0.0v
	 *
	 *
	 */


public class StringUppercase {
		
		
		/**
		 * This is the main method of this programme
		 * @param args for input arguments
		 */
		
		public static void main(String[] args) {
			
			/** String variable, string of the word
			 * 
			 */
			
			String str = "hi prashajini!";

			String strUpperCase = str.toUpperCase();

			System.out.println(strUpperCase);
		
			/**Prints as Uppercase
			 * 
			 */
	}
	}


