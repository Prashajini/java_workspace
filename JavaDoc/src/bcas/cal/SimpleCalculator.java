package bcas.cal;


	/**
	 * To perform basic calculator functions
	 * 
	 * @author Prashajini
	 * @since 18.05.2020
	 * @version 2.0.0v
	 *
	 *
	 */

	public class SimpleCalculator {
		
		/**
		 * variable a, number 1 value for addition
		 */
		
		public int a;
		
		/**
		 * variable b, number 2 value for addition
		 */
		public int b;
		
		/**
		 * 
		 * @param a adding first value
		 * @param b adding second value 
		 * @return (a+b)
		 */
		
		public int sum(int a, int b) {
			
			return a+b;
		}
		
		/**
		 * This is the main method of this programme
		 * @param args for input arguments
		 */
		
		public static void main(String[] args) {
			
			SimpleCalculator cal = new SimpleCalculator();
			System.out.println(cal.sum(15, 15));

	}
	}





